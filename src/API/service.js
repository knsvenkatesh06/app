import React, { Component } from 'react'
import axios from 'axios';
import {
    Row,
    img,
    Col,
    Container,

} from "reactstrap";
class Service extends Component {
    constructor(props) {
        super(props)
        this.state = {
            service: null
        }
    }
    async GetService() {
        await axios.get(`https://api.inquickerstaging.com/v3/winter.inquickerstaging.com/services`

        ).then(resp => {
            if (resp.data != undefined && resp.data.data != undefined) {
                console.log(resp);
                this.setState({
                    service: resp.data.data
                })
            }
        }).catch(err => {
            console.log(err);
        });

    }
    async componentDidMount() {
        await this.GetService();
    }
    render() {
        if (this.state.service == null)
            return (
                <Container>
                    <Row>
                        <Col>
                        </Col>
                        <Col>
                            <div >
                                <p>Services loading</p>
                            </div>
                        </Col>
                        <Col>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={3} xs={3}>

                        </Col>
                    </Row>
                </Container>

            )
        return (
            <>
                <Container>
                    <Row>
                        <Col>
                        </Col>
                        <Col>
                            <div >
                                <p>Services</p>
                            </div>
                        </Col>
                        <Col>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={3} xs={3} >
                            {this.state.service.map((item, i) => {


                                return (
                                    <div key={i}>
                                        <a>
                                            {item.id}
                                        </a>

                                    </div>
                                )


                            })

                            }
                        </Col>
                    </Row>
                </Container>

            </>
        )
    }
}
export default Service;
