import React, { Component } from 'react'
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
    Row,
    img,
    Col,
    Container,

} from "reactstrap";
class Provider extends Component {
    constructor(props) {
        super(props)
        this.state = {
            service: null
        }
    }
    async GetProviders() {
        await axios.get(`https://api.inquickerstaging.com/v3/winter.inquickerstaging.com/providers?include=locations%2Cschedules.location&page%5Bnumber%5D=1&page%5Bsize%5D=10`

        ).then(resp => {
            if (resp.data != undefined && resp.data.data != undefined) {
                //  console.log(resp);
                this.setState({
                    service: resp.data.data
                })
            }
        }).catch(err => {
            console.log(err);
        });

    }
    async componentDidMount() {
        await this.GetProviders();
    }
    render() {
        if (this.state.service == null)
            return (
                <div >
                    <p>Providers loading</p>
                </div>
            )
        return (
            <div>
                <div>Provider</div>
                <Container>
                    {this.state.service.map((item, i) => {
                        return (
                            <div id={i}>

                                <Row key={i}>

                                    <Col lg={3} xs={3}>
                                        <Row>


                                            <Col lg={3} xs={3}>
                                                <img src={item.attributes["profile-image"]} alt={item.attributes.name} />
                                            </Col>
                                            <Col lg={3} xs={3}>
                                            </Col>
                                        </Row>

                                    </Col>
                                    <Col lg={9} xs={9}>
                                        <Col lg={1} xs={1}>

                                        </Col>

                                        <Col lg={3} xs={3}>

                                            <div>

                                             Name:   {item.attributes.name}
                                            </div>
                                            <div> subspecialties</div>
                                            <div>
                                             
                                                {item.attributes.subspecialties.map((spe, i) => {
                                                 return   <div key={i}>
                                                        {spe}
                                                    </div>

                                                })}
                                            </div>
                                        </Col>

                                    </Col>
                                </Row>

                            </div>
                        )
                    })



                    }
                </Container>
            </div>
        )
    }
}

export default Provider;