import React from 'react';
import logo from './logo.svg';
import './App.css';
import Service from './API/service';
import Provider from './API/provider';


function App() {
  return (
    <div className="App">
          
        <Service/>
        <Provider/>
    </div>
  );
}

export default App;
